# Improvement in read mapping for segmental duplications using the DuploMap
# algorithm on simulated data.
# (A) Comparison of different long-read alignment tools.
# (B) Impact of read length on mappability.
# (C) Improvement in mappability stratified by sequence similarity of segmental duplications.
# (D) Improvement in mappability for 9 disease-associated genes that are located in segmental
#     duplications.

library(ggplot2)
library(plyr) # revalue
library(cowplot) # plot_grid
library(gridExtra) # grid.arrange

load_bam_eval <- function(filename) {
  eval <- read.csv(filename, sep='\t', comment.char='#')
  eval$tool <- factor(revalue(sapply(as.character(eval$bam_flag),
                                    function(x) unlist(strsplit(x, '_'))[1]),
          c('minimap' = 'Minimap2', 'ngmlr' = 'NGMLR', 'blasr' = 'BLASR')))
  eval$realigned <- factor(revalue(sapply(as.character(eval$bam_flag),
                                          function(x) unlist(strsplit(x, '_'))[2]),
                                   c('before' = 'Original', 'after' = 'DuploMap')),
                           levels=c('Original', 'DuploMap'))
  eval$bam_flag <- NULL
  eval <- within(eval, high_mapq_p <- 100 * high_mapq / total)
  eval <- within(eval, correct_p <- 100 * correct / total)
  eval <- within(eval, high_correct_p <- 100 * high_correct / high_mapq)
  eval
}

parse_regions <- function(eval, thresholds) {
  eval <- eval[eval$tool == 'Minimap2' & eval$region != 'similar',]
  eval$region <- as.character(sapply(as.character(eval$region),
                                     function(x) unlist(strsplit(x, '\\.'))[2]))
  eval$sim_lower <- as.numeric(sapply(as.character(eval$region),
                                      function(x) unlist(strsplit(x, '-'))[1]))
  eval$sim_upper <- as.numeric(sapply(as.character(eval$region),
                                      function(x) unlist(strsplit(x, '-'))[2]))

  eval$bin <- 'None'
  for (i in 1:(length(thresholds) - 1)) {
    x <- thresholds[i]
    y <- thresholds[i + 1]
    eval[eval$sim_lower >= x & eval$sim_upper <= y,]$bin <-
      sprintf('%.1f - %.1f', x / 10, y / 10)
  }
  eval <- eval[eval$bin != 'None',]
  eval$bin <- factor(eval$bin)
  aggr <- aggregate(cbind(total, high_mapq, correct, high_correct)
                    ~ realigned + tool + bin, eval, sum)
  aggr
}

colors <- c('#ED923C', '#2F5999')
dodge <- position_dodge(width=.7)

plot_grid_wrap <- function(g1, g2, ...) {
  g1 <- ggplotGrob(g1)
  g2 <- ggplotGrob(g2)
  max.width = grid::unit.pmax(g1$widths[2:5], g2$widths[2:5])
  g1$widths[2:5] <- as.list(max.width)
  g2$widths[2:5] <- as.list(max.width)
  plot_grid(g1, g2, ...)
}

########################################################
# (A) Comparison of different long-read alignment tools.
########################################################
# Data:
#     data/hg37_sim_cov30/hg37.bam_eval.csv
# Output:
#     plots/figure2/a.png

eval <- load_bam_eval('data/hg37_sim_cov30/hg37.bam_eval.csv')
eval <- eval[eval$region == 'similar', ]

(g1 <- ggplot(eval) +
    geom_bar(aes(tool, high_mapq_p, fill=realigned),
             position=dodge, color='black', size=.2,
             stat='summary', fun.y='mean', width=.7) +
    scale_y_continuous('Reads with MAPQ >= 30 (%)', breaks=seq(0, 100, 10)) +
    scale_fill_manual('', values=colors) +
    theme_light() +
    theme(axis.title.x=element_blank(),
          axis.text.x=element_blank(),
          axis.title.y=element_text(size=9),
          legend.position='top',
          legend.margin=margin(0,0,0,0),
          legend.key.size=unit(0.03, 'snpc')))
(g2 <- ggplot(eval) +
    geom_bar(aes(tool, high_correct_p, fill=realigned, alpha='0'),
             position=dodge, color='black', size=.2, stat='summary', fun.y='mean',
             width=.7) +
    geom_bar(aes(tool, correct_p, fill=realigned, alpha='1'),
             position=dodge, color='black', size=.2, stat='summary', fun.y='mean', width=.7) +
    scale_y_continuous('Correctly mapped reads (%)', breaks=seq(0, 100, 5)) +
    coord_cartesian(ylim=c(70, 100)) +
    scale_fill_manual('', values=colors) +
    scale_alpha_manual('', labels=c('Reads with MAPQ >= 30  ', 'All reads'),
                       values=c(0.5, 1)) +
    theme_light() +
    guides(fill=F) +
    theme(axis.title.x=element_blank(),
          axis.text.x=element_text(size=8),
          axis.title.y=element_text(size=9),
          axis.text.y=element_text(face=c('bold', rep('plain', 10))),
          legend.position='bottom',
          legend.margin=margin(0,0,0,0),
          legend.key.size=unit(0.03, 'snpc')))
(panel_a <- plot_grid_wrap(g1, g2, rel_heights=c(1, 1), ncol=1))
ggsave('plots/figure2/a.png', panel_a, width=8, height=8, scale=0.8)

###########################################
# (B) Impact of read length on mappability.
###########################################
# Data:
#     data/hg37_sim_{,len20kb_,len50kb_}cov30/hg37.bam_eval.csv
# Output:
#     plots/figure2/b.png

eval_tmp <- load_bam_eval('data/hg37_sim_cov30/hg37.bam_eval.csv')
eval_tmp$read_length <- '8.5kb'
eval_tmp <- eval_tmp[eval_tmp$region == 'similar' & eval_tmp$tool == 'Minimap2',]
eval_tmp$tool <- factor(eval_tmp$tool)
eval <- eval_tmp

eval_tmp <- load_bam_eval('data/hg37_sim_len20kb_cov30/hg37.bam_eval.csv')
eval_tmp$read_length <- '20kb'
eval <- rbind(eval, eval_tmp[eval_tmp$region == 'similar',])
eval_tmp <- load_bam_eval('data/hg37_sim_len50kb_cov30/hg37.bam_eval.csv')
eval_tmp$read_length <- '50kb'
eval <- rbind(eval, eval_tmp[eval_tmp$region == 'similar',])
eval$read_length <- factor(eval$read_length, levels=c('8.5kb', '20kb', '50kb'))
eval$realigned <- revalue(eval$realigned, c('Original'='Minimap2'))

(g1 <- ggplot(eval) +
    geom_bar(aes(read_length, high_mapq_p, fill=realigned),
             position=dodge, color='black', size=.2,
             stat='summary', fun.y='mean', width=.7) +
    scale_y_continuous('Reads with MAPQ >= 30 (%)', breaks=seq(0, 100, 10)) +
    scale_fill_manual('', values=colors) +
    theme_light() +
    theme(axis.title.x=element_blank(),
          axis.text.x=element_blank(),
          axis.title.y=element_text(size=9),
          legend.position='top',
          legend.margin=margin(0,0,0,0),
          legend.key.size=unit(0.03, 'snpc')))
(g2 <- ggplot(eval) +
    geom_bar(aes(read_length, high_correct_p, fill=realigned, alpha='0'),
             position=dodge, color='black', size=.2, stat='summary', fun.y='mean',
             width=.7) +
    geom_bar(aes(read_length, correct_p, fill=realigned, alpha='1'),
             position=dodge, color='black', size=.2, stat='summary', fun.y='mean', width=.7) +
    scale_x_discrete('Read length') +
    scale_y_continuous('Correctly mapped reads (%)', breaks=90:100) +
    coord_cartesian(ylim=c(90, 100)) +
    scale_fill_manual('', values=colors) +
    scale_alpha_manual('', labels=c('Reads with MAPQ >= 30  ', 'All reads'),
                       values=c(0.5, 1)) +
    theme_light() +
    guides(fill=F) +
    theme(axis.title.x=element_text(size=9),
          axis.text.x=element_text(size=8),
          axis.title.y=element_text(size=9),
          axis.text.y=element_text(face=c('bold', rep('plain', 10))),
          legend.position='bottom',
          legend.margin=margin(0,0,0,0),
          legend.key.size=unit(0.03, 'snpc')))

(panel_b <- plot_grid_wrap(g1, g2, rel_heights=c(1, 1), ncol=1))
ggsave('plots/figure2/b.png', panel_b, width=8, height=8, scale=0.8)

#############################################################################################
# (C) Improvement in mappability stratified by sequence similarity of segmental duplications.
#############################################################################################
# Data:
#     data/hg37_sim_cov30/hg37.bam_eval.csv
# Output:
#     plots/figure2/c.png

thresholds <- c(950, 980, 990, 995, 999, 1000)
eval <- load_bam_eval('data/hg37_sim_cov30/hg37.bam_eval.csv')
eval <- parse_regions(eval, thresholds)
eval <- within(eval, high_mapq_p <- 100 * high_mapq / total)
eval <- within(eval, correct_p <- 100 * correct / total)
eval <- within(eval, high_correct_p <- 100 * high_correct / high_mapq)
eval$realigned <- revalue(eval$realigned, c('Original'='Minimap2'))

(g1 <- ggplot(eval) +
    geom_bar(aes(bin, high_mapq_p, fill=realigned),
             position=dodge, color='black', size=.2,
             stat='summary', fun.y='mean', width=.7) +
    scale_y_continuous('Reads with MAPQ >= 30 (%)', breaks=seq(0, 100, 10)) +
    scale_fill_manual('', values=colors) +
    theme_light() +
    theme(axis.title.x=element_blank(),
          axis.text.x=element_blank(),
          axis.title.y=element_text(size=9),
          legend.position='top',
          legend.margin=margin(0,0,0,0),
          legend.key.size=unit(0.03, 'snpc')))
(g2 <- ggplot(eval) +
    geom_bar(aes(bin, high_correct_p, fill=realigned, alpha='0'),
             position=dodge, color='black', size=.2, stat='summary', fun.y='mean',
             width=.7) +
    geom_bar(aes(bin, correct_p, fill=realigned, alpha='1'),
             position=dodge, color='black', size=.2, stat='summary', fun.y='mean', width=.7) +
    scale_x_discrete('Sequence similarity (%)') +
    scale_y_continuous('Correctly mapped reads (%)', breaks=seq(80, 100, 2)) +
    coord_cartesian(ylim=c(80, 100)) +
    scale_fill_manual('', values=colors) +
    scale_alpha_manual('', labels=c('Reads with MAPQ >= 30  ', 'All reads'),
                       values=c(0.5, 1)) +
    theme_light() +
    guides(fill=F) +
    theme(axis.title.x=element_text(size=9),
          axis.text.x=element_text(size=8),
          axis.title.y=element_text(size=9),
          axis.text.y=element_text(face=c('bold', rep('plain', 10))),
          legend.position='bottom',
          legend.margin=margin(0,0,0,0),
          legend.key.size=unit(0.03, 'snpc')))

(panel_c <- plot_grid_wrap(g1, g2, rel_heights=c(1, 1), ncol=1))
ggsave('plots/figure2/c.png', panel_c, width=8, height=8, scale=0.8)

###############################################################
# (D) Improvement in mappability for 9 disease-associated genes
# that are located in segmental duplications.
###############################################################
# Input: realignment/hg37_sim_cov30/comparison/genes.csv
# Output: plots/figure2/d.png

genes <- read.csv('realignment/hg37_sim_cov30/comparison/genes.csv', sep='\t',
                  comment.char='#')
genes <- within(genes, high_mapq_p <- high_mapq * 100 / total)
genes <- within(genes, correct_p <- correct * 100 / total)
genes <- within(genes, high_correct_p <- high_correct * 100 / high_mapq)
genes$tool <- factor(revalue(genes$tool,
                             c('before'='Minimap2', 'after'='DuploMap')),
                     levels=c('Minimap2', 'DuploMap'))
only_duplomap <- genes[genes$tool == 'DuploMap',]
genes$gene <- factor(genes$gene,
                     levels=only_duplomap[order(only_duplomap$high_mapq_p), 'gene'])

(g1 <- ggplot(genes) +
    geom_bar(aes(gene, high_mapq_p, fill=tool),
             position=dodge, color='black', size=.2,
             stat='summary', fun.y='mean', width=.7) +
    scale_y_continuous('Reads with MAPQ >= 30 (%)', breaks=seq(0, 100, 10), limits=c(0, 105)) +
    scale_fill_manual('', values=colors) +
    theme_light() +
    theme(axis.title.x=element_blank(),
          axis.text.x=element_blank(),
          axis.title.y=element_text(size=9),
          legend.position='top',
          legend.margin=margin(0,0,0,0),
          legend.key.size=unit(0.03, 'snpc')))
(g2 <- ggplot(genes) +
    geom_bar(aes(gene, high_correct_p, fill=tool, alpha='0'),
             position=dodge, color='black', size=.2, stat='summary', fun.y='mean',
             width=.7) +
    geom_bar(aes(gene, correct_p, fill=tool, alpha='1'),
             position=dodge, color='black', size=.2, stat='summary', fun.y='mean', width=.7) +
    scale_y_continuous('Correctly mapped reads (%)',
                       breaks=90:100) +
    coord_cartesian(ylim=c(90, 100)) +
    scale_fill_manual('', values=colors) +
    scale_alpha_manual('', labels=c('Reads with MAPQ >= 30  ', 'All reads'),
                       values=c(0.5, 1)) +
    theme_light() +
    guides(fill=F) +
    theme(axis.title.x=element_blank(),
          axis.text.x=element_text(size=8),
          axis.title.y=element_text(size=9),
          axis.text.y=element_text(face=c('bold', rep('plain', 10))),
          legend.position='bottom',
          legend.margin=margin(0,0,0,0),
          legend.key.size=unit(0.03, 'snpc')))

(panel_d <- plot_grid_wrap(g1, g2, rel_heights=c(1, 1), ncol=1))
ggsave('plots/figure2/d.png', panel_d, width=8, height=8, scale=0.8)


plot_grid(panel_a, panel_b, panel_c, panel_d,
          labels=sprintf('(%s)', letters),
          label_fontface='plain', label_size=15)
ggsave('plots/figure2/figure2.png', width=16, height=16, scale=0.6)
