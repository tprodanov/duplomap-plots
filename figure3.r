# Analysis of read mappability on whole-genome long-read datasets for HG002.
# (a) Whole-genome read mappability for DuploMap compared to Minimap2
# for PacBio CCS, PacBio CLR and  ONT read datasets for HG002.
# (b) Improvement in coverage for medically-relevant disease genes that
# are difficult to map using short reads.
# Only genes that were less than 100% mappable using Minimap2 alignments are shown.

library(ggplot2)
library(tidyr) # spread
library(dplyr) # rename
library(plyr) # revalue

colors <- c('#ED923C', '#2F5999')
LENGTH <- c('hg37'=2858658097, 'hg38'=2923716080)
COVERAGE <- 10

#####################################################################
# (a) Whole-genome read mappability for DuploMap compared to Minimap2
# for PacBio CCS, PacBio CLR and  ONT read datasets for HG002.
#####################################################################

load_coverage <- function(filename, technology, genome) {
  tmp <- read.csv(filename, sep='\t')
  tmp <- aggregate(clen ~ mapq + flag, tmp[tmp$coverage >= COVERAGE,], sum)
  tmp$flag <- factor(revalue(tmp$flag, c('before'='Minimap2', 'after'='DuploMap')),
                     levels=c('Minimap2', 'DuploMap'))
  tmp$technology <- technology
  tmp$genome <- genome
  tmp
}

cov <- data.frame()
cov <- rbind(cov, load_coverage('coverage/HG002/hg38.minimap.total.bed', 'CLR', 'hg38'))
cov <- rbind(cov, load_coverage('coverage/HG002/hg38.ccs.total.bed', 'CCS', 'hg38'))
cov <- rbind(cov, load_coverage('coverage/HG002/hg38.ultralong.total.bed',
                                'Ultralong', 'hg38'))
cov$perc <- cov$clen * 100 / LENGTH[cov$genome]

ggplot(cov[cov$mapq <= 60 & cov$genome == 'hg38',]) +
  geom_line(aes(mapq, perc, linetype=flag, color=technology)) +
  geom_text(aes(-1, perc, label=technology),
            data=cov[cov$mapq == 0 & cov$flag == 'DuploMap',], hjust=1) +
  scale_x_continuous('Mapping quality', limits=c(-7, 60), breaks=seq(0, 60, 10)) +
  scale_y_continuous('Bases covered by at least 10 reads (%)',
                     limits=c(94, 100), breaks=94:100) +
  scale_color_manual('Technology', values=c('green4', 'red3', 'blue3')) +
  scale_linetype_manual('Alignment', values=c('dashed', 'solid')) +
  theme_light()
ggsave('plots/figure3/a.png', width=16, height=8, scale=0.5)

###################################################################################
# (b) Improvement in coverage for medically-relevant disease genes that
# are difficult to map using short reads.
# Only genes that were less than 100% mappable using Minimap2 alignments are shown.
###################################################################################
# Input: coverage/HG002/hg38.minimap.genes.csv
# Output: plots/figure3/b.png

genes <- read.csv('coverage/HG002/hg38.minimap.genes.csv', sep='\t', comment.char='#')
genes <- rename(genes, gene=key1, region_type=key2)

genes_wide <- spread(genes[genes$region_type == 'exon',], flag, covered_len)
genes_wide$before_p <- 100 * genes_wide$before / genes_wide$total_len
genes_wide$after_p <- 100 * genes_wide$after / genes_wide$total_len
genes_wide$diff <- genes_wide$after_p - genes_wide$before_p
# Should be empty!
genes_wide[genes_wide$diff < 0,]

genes_nonzero <- genes_wide[genes_wide$diff != 0,]
genes_nonzero <- genes_nonzero[order(genes_nonzero$diff),]
genes_nonzero$gene <- factor(genes_nonzero$gene, levels=genes_nonzero$gene)

ggplot(genes_nonzero) +
  geom_bar(aes(gene, after_p, fill='1'), stat='identity', position='dodge') +
  geom_bar(aes(gene, before_p, fill='0'), stat='identity', position='dodge') +
  geom_text(aes(gene, before_p / 2,
                label=ifelse(before_p > 4, sprintf('%.0f', before_p), '')),
                size=3) +
  geom_text(aes(gene, ifelse(diff >= 4, before_p + diff / 2, after_p + 3),
                label=sprintf('%.0f', diff),
                color=ifelse(diff < 4, 'black', 'white')), size=3) +
  scale_y_continuous('Bases covered by at least 10 reads (%)') +
  scale_fill_manual('', values=colors, labels=c('Minimap2', 'Improvement')) +
  scale_color_manual('', values=c('black', 'white')) +
  guides(color=F) +
  theme_light() +
  theme(axis.title.x=element_blank(), axis.text.x=element_text(angle=90, hjust=1, vjust=0.5),
        legend.position='top')
ggsave('plots/figure3/b.png', width=16, height=8, scale=0.5)
